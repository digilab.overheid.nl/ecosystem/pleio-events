const btn = document.getElementById('filter');
const container = document.getElementById('filter-container');

let visible = false;

btn.addEventListener('click', () => {
  container.classList.toggle('visible');
});

// When clicked outside, set the container to invisible
document.addEventListener('click', function (ev) {
  if (!ev.target.closest('#filter-container')) {
    container.classList.remove('visible');
  }
});
