package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

const baseURL = "https://digilab.pleio.nl/graphql" // IMPROVE: from environment variable?

// Request performs a simple http request to the given path and unmarshals the result to the specified pointer. Used instead of GraphQL libraries, since the events are relatively simple
func Request(method string, data interface{}, v interface{}) (err error) {
	client := http.Client{}
	var req *http.Request

	var jsonData []byte
	if data != nil {
		if jsonData, err = json.Marshal(data); err != nil {
			return
		}
	}

	if req, err = http.NewRequest(method, baseURL, bytes.NewBuffer(jsonData)); err != nil {
		return
	}

	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "*/*")
	req.Header.Set("Cache-Control", "no-cache")
	// req.AddCookie(&http.Cookie{
	// 	Name:  "sessionid",
	// 	Value: "abc", // Note: must be a valid Django session ID from https://gitlab.com/pleio/backend2, seems impossible to receive from an OIDC access token
	// })
	// req.Header.Set("Authorization", "Bearer xyz") // Note: doesn't work

	// Do the request
	var resp *http.Response
	if resp, err = client.Do(req); err != nil {
		return
	}

	if resp.StatusCode != 200 {
		body, _ := io.ReadAll(resp.Body)
		err = fmt.Errorf("response returned with error code %v. response body: %v", resp.StatusCode, string(body))
		return
	}

	// Read the response body and possible error
	var result []byte
	if result, err = io.ReadAll(resp.Body); err != nil {
		return
	}

	// log.Println("--->")
	// log.Printf("%s", result)

	// If v is set, unmarshal the response to v (which should be a pointer)
	if v != nil {
		err = json.Unmarshal(result, v)
	}

	return
}
