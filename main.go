package main

import (
	"flag"
	"fmt"
	"log"
	"pleio-events/api"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/html/v2"
)

// Define day and month abbreviations. Note: stored using string keys for faster usage in the for loops below
var dayAbbreviations = map[string]string{
	time.Monday.String():    "ma",
	time.Tuesday.String():   "di",
	time.Wednesday.String(): "wo",
	time.Thursday.String():  "do",
	time.Friday.String():    "vr",
	time.Saturday.String():  "za",
	time.Sunday.String():    "zo",
}

var monthAbbreviations = map[string]string{
	time.January.String():   "jan",
	time.February.String():  "feb",
	time.March.String():     "mrt",
	time.April.String():     "apr",
	time.May.String():       "mei",
	time.June.String():      "jun",
	time.July.String():      "jul",
	time.August.String():    "aug",
	time.September.String(): "sep",
	time.October.String():   "okt",
	time.November.String():  "nov",
	time.December.String():  "dec",
}

func main() {
	// Parse the optional port flag
	port := flag.Int("port", 80, "port on which to run the web server")
	flag.Parse()

	// Template engine and functions
	engine := html.New("./views", ".html")
	engine.AddFuncMap(template.FuncMap{
		"timeToDay": func(t time.Time) string {
			return strconv.Itoa(t.Day())
		},
		"timeToMonth": func(t time.Time) string {
			return monthAbbreviations[t.Month().String()]
		},
		"timeToDate": func(t time.Time) string { // Output: e.g. 'wo 2 aug 2023'
			res := t.Format("Monday 2 January 2006")

			// Translate the language to Dutch using string replacement. IMPROVE: use a library for this?
			for weekday, replacement := range dayAbbreviations {
				res = strings.Replace(res, weekday, replacement, 1)
			}

			for month, replacement := range monthAbbreviations {
				res = strings.Replace(res, month, replacement, 1)
			}

			return res
		},
		"timeToTime": func(t time.Time) string {
			return t.Format("15:04")
		},
	})

	// Fiber instance
	app := fiber.New(fiber.Config{
		Views:       engine,
		ViewsLayout: "layouts/main",
	})

	// Middleware
	app.Use(logger.New(logger.Config{
		Format: "${time} | ${status} | ${latency} | ${method} | ${path}   ${error}\n", // Do not log IP addresses. Note: see https://docs.gofiber.io/api/middleware/logger#constants for more logger variables
	}))

	// Note: not using CORS middleware, since already implemented in the ingress

	// Routes
	app.Static("/", "./public")

	// security.txt redirect
	app.Get("/.well-known/security.txt", func(c *fiber.Ctx) error {
		return c.Redirect("https://www.ncsc.nl/.well-known/security.txt", fiber.StatusFound) // StatusFound is HTTP code 302
	})

	// app.Get("/callback", func(c *fiber.Ctx) error {
	// 	log.Println("callback message sent")

	// 	// TODO: validate code and state

	// 	return nil
	// })

	// app.Get("/login", func(c *fiber.Ctx) error {
	// 	ctx := context.Background()

	// 	conf := &oauth2.Config{
	// 		ClientID:     "", // TODO: env
	// 		ClientSecret: "", // TODO: env
	// 		Scopes:       []string{"openid", "profile", "email"},
	// 		Endpoint: oauth2.Endpoint{
	// 			AuthURL:  "https://account.pleio.nl/openid/authorize/",
	// 			TokenURL: "https://account.pleio.nl/openid/token/",
	// 		},
	// 		RedirectURL: "https://authentik.tooling.digilab.network/source/oauth/callback/pleio/",
	// 	}

	// 	// conf := &oauth2.Config{
	// 	// 	ClientID:     "", // TODO: env
	// 	// 	ClientSecret: "", // TODO: env
	// 	// 	Scopes:       []string{"openid", "profile", "email"},
	// 	// 	Endpoint: oauth2.Endpoint{
	// 	// 		AuthURL:  "https://authentik.tooling.digilab.network/application/o/authorize/",
	// 	// 		TokenURL: "https://authentik.tooling.digilab.network/application/o/token/",
	// 	// 	},
	// 	// 	RedirectURL: "https://digilab.overheid.nl/callback",
	// 	// }

	// 	// Redirect user to consent page to ask for permission
	// 	// for the scopes specified above.
	// 	url := conf.AuthCodeURL("state", oauth2.AccessTypeOffline) // TODO: set proper state
	// 	fmt.Printf("Visit the URL for the auth dialog: %v", url)

	// 	// Use the authorization code that is pushed to the redirect
	// 	// URL. Exchange will do the handshake to retrieve the
	// 	// initial access token. The HTTP Client returned by
	// 	// conf.Client will refresh the token as necessary.
	// 	var code string
	// 	if _, err := fmt.Scan(&code); err != nil {
	// 		log.Fatal(err)
	// 	}

	// 	fmt.Println("--> trying code:", code)

	// 	tok, err := conf.Exchange(ctx, code)
	// 	if err != nil {
	// 		log.Fatal(err)
	// 	}

	// 	fmt.Println("received token:", tok)

	// 	client := conf.Client(ctx, tok)
	// 	resp, err := client.Get("https://account.pleio.nl/openid/userinfo/")
	// 	// resp, err := client.Get("https://authentik.tooling.digilab.network/application/o/userinfo/")
	// 	if err != nil {
	// 		return err
	// 	}

	// 	respBytes, err := io.ReadAll(resp.Body)
	// 	if err != nil {
	// 		return err
	// 	}

	// 	fmt.Printf("response: %s", respBytes)

	// 	return c.JSON("hello world")
	// })

	app.Get("/", func(c *fiber.Ctx) error {
		// var isPrevious bool
		filter := "upcoming"
		// if c.Query("filter") == "previous" {
		// 	isPrevious = true
		// 	filter = "previous"
		// }

		// IMPROVE: log in using some user to access non-public events and detailed attendees?

		// Note: see https://gitlab.com/pleio/backend2/-/blob/dev/backend2/schema.graphql for the GraphQL schema

		// Fetch the events using the GraphQL Pleio API
		var res struct { // Note: e.g. https://mholt.github.io/json-to-go/ can be used to generate the struct below
			Data struct {
				Activities struct {
					Total int `json:"total"`
					Edges []struct {
						GUID   string `json:"guid"`
						Type   string `json:"type"`
						Entity struct {
							GUID          string `json:"guid"`
							Title         string `json:"title"`
							Excerpt       string `json:"excerpt"`
							CanEdit       bool   `json:"canEdit"`
							Subtype       string `json:"subtype"`
							URL           string `json:"url"`
							Votes         any    `json:"votes"`
							HasVoted      any    `json:"hasVoted"`
							IsBookmarked  bool   `json:"isBookmarked"`
							IsPinned      bool   `json:"isPinned"`
							InGroup       bool   `json:"inGroup"`
							CanBookmark   bool   `json:"canBookmark"`
							TagCategories []any  `json:"tagCategories"`
							Rsvp          bool   `json:"rsvp"`
							IsFeatured    bool   `json:"isFeatured"`
							Featured      struct {
								Image      any    `json:"image"`
								Video      any    `json:"video"`
								VideoTitle string `json:"videoTitle"`
								PositionY  int    `json:"positionY"`
								Alt        string `json:"alt"`
								Typename   string `json:"__typename"`
							} `json:"featured"`
							StartDate       time.Time `json:"startDate"`
							EndDate         time.Time `json:"endDate"`
							Location        string    `json:"location"`
							LocationLink    string    `json:"locationLink"`
							TimePublished   time.Time `json:"timePublished"`
							StatusPublished string    `json:"statusPublished"`
							CommentCount    int       `json:"commentCount"`
							CanComment      bool      `json:"canComment"`
							Owner           struct {
								GUID     string `json:"guid"`
								Username string `json:"username"`
								Name     string `json:"name"`
								URL      string `json:"url"`
								Icon     string `json:"icon"`
								Roles    any    `json:"roles"`
								Vcard    []any  `json:"vcard"`
								Typename string `json:"__typename"`
							} `json:"owner"`
							IsAttending any `json:"isAttending"`
							Attendees   struct {
								TotalAccept int    `json:"totalAccept"`
								Edges       []any  `json:"edges"`
								Typename    string `json:"__typename"`
							} `json:"attendees"`
							Group    any    `json:"group"`
							Typename string `json:"__typename"`
						} `json:"entity"`
						Typename string `json:"__typename"`
					} `json:"edges"`
					Typename string `json:"__typename"`
				} `json:"activities"`
			} `json:"data"`
		}

		// Do an API request and parse the results
		err := api.Request("POST", map[string]interface{}{
			"operationName": "ActivityList",
			"variables": map[string]interface{}{
				"subtypes": []string{
					"event",
				},
				"tagCategories":  []string{},
				"orderBy":        "startDate",
				"orderDirection": "asc",
				"offset":         0,
				"limit":          20,
				"eventFilter":    filter,
			},
			"query": `query ActivityList($containerGuid: String, $offset: Int!, $limit: Int!, $subtypes: [String!], $groupFilter: [String!], $tagCategories: [TagCategoryInput!], $orderBy: OrderBy, $orderDirection: OrderDirection, $sortPinned: Boolean, $statusPublished: [StatusPublished], $userGuid: String, $eventFilter: EventFilter) {
					activities(
					offset: $offset
					limit: $limit
					containerGuid: $containerGuid
					tagCategories: $tagCategories
					subtypes: $subtypes
					groupFilter: $groupFilter
					orderBy: $orderBy
					orderDirection: $orderDirection
					sortPinned: $sortPinned
					statusPublished: $statusPublished
					userGuid: $userGuid
					eventFilter: $eventFilter
				) {
						total
					edges {
						guid
					type
					entity {
							guid
						...EventListFragment
						__typename
							}
					__typename
						}
					__typename
					}
				}

				fragment EventListFragment on Event {
					guid
				title
				excerpt
				canEdit
				subtype
				url
				votes
				hasVoted
				isBookmarked
				isPinned
				inGroup
				canBookmark
				tagCategories {
						values
					__typename
					}
				rsvp
				isFeatured
				featured {
						image
					video
					videoTitle
					positionY
					alt
					__typename
					}
				startDate
				endDate
				location
				locationLink
				timePublished
				statusPublished
				commentCount
				canComment
				owner {
						guid
					username
					name
					url
					icon
					roles
					vcard {
						guid
					name
					value
					__typename
						}
					__typename
					}
				isAttending
				attendees(limit: 3, state: "accept", orderBy: timeUpdated, orderDirection: desc) {
						totalAccept
					edges {
						guid
					icon
					url
					name
					state
					__typename
						}
					__typename
					}
				group {
						guid
					... on Group {
						name
					url
					membership
					isMembershipOnRequest
					__typename
						}
					__typename
					}
				__typename
				}
			`,
		}, &res)
		if err != nil {
			log.Println(err)
		}

		// Return the results as JSON. Note: the templates are currently not used
		return c.JSON(res.Data.Activities.Edges)

		// // Show the results using templates
		// return c.Render("index", fiber.Map{
		// 	"isPrevious": isPrevious,
		// 	"events":     res.Data.Activities.Edges,
		// })
	})

	// Start server
	if err := app.Listen(fmt.Sprintf(":%d", *port)); err != nil { // Note: port should never be nil, since flag.Parse() is run
		log.Println("error starting server:", err)
	}
}
