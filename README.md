# pleio-events

Page that shows the content of the [Digilab Pleio events page](https://digilab.pleio.nl/events), using the same styles, but with simplified layout, so it can be embedded on other pages.
